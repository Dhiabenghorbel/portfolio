import React from "react";
import "./App.css";
import Home from "./Components/Home/Home.tsx";
import ScrollTop from "./Components/ScrollTop/ScrollTop.tsx";
// import { BrowserRouter as Router, Route, Routes } from "react-router-dom";
// import ShoppingCart from "./Components/ShoppingCart/ShoppingCart.tsx";
import AboutBar from "./Components/About/AboutBar.tsx";
// import Header from "./Components/Header/Header.tsx";
function App() {
  return (
    // <Router>
    //   <ScrollTop />
    //   <div className="App-header">
    //   {/* <Header /> */}
    //     <Routes>
    //       <Route path="/" element={<Home />} />
    //       <Route path="/shoppingCart" element={<ShoppingCart />} />
    //     </Routes>
    //   </div>
    //
    // </Router>
    <div>
      <ScrollTop />
      <div className="App-header">
        <Home />
      </div>
      <AboutBar />
    </div>
  );
}

export default App;
