import React from "react";
import "./ScrollTop.scss";
import Icon from "../Icon/Icon.tsx";

const ScrollTop = ({}) => {
  const scrollPosition = window.scrollY;

  const BackTop = () => {
    window.scrollTo(0, 0);
  };
  return (
    <React.Fragment>
      <div className={"scroll-top"}>
        <div className={"scroll-top-button"} onClick={BackTop}>
          <Icon
            name="fa-solid fa-chevron-up"
            medium
            light
            style={{
              display: "flex",
              justifyContent: "center",
              backgroundColor: "transparent",
            }}
          />
        </div>
      </div>
    </React.Fragment>
  );
};

export default ScrollTop;
