import React from "react";
import "./Icon.scss";
import classnames from 'classnames';
import "../../styles/commons.scss";
interface IconProps {
  id?: string;
  style?: string;
  name: string;
  onClick?: Function;
  disabled?: boolean;
  light?: boolean;
  link?: boolean;
  medium:boolean;
  big?: boolean; 
  src:string; 
  download?: boolean; 
}


const Icon = ({ id, style, name, onClick, disabled, light, medium, big, link, src, download }: IconProps) => {
  return (
    <span
      id={id}
      onClick={(e) => (disabled ? null : onClick ? onClick(e) : null)}
    >
      <a
        href={src}
        target="_blank"
        rel="noreferrer"
        download={download ? true : undefined}
      >
        <i
          className={classnames(
            "icon-default",
            `${name}`,
            { "text-white": light },
            { "text-disabled": disabled },
            { "text-black": !light },
            { "text-link cursor-pointer": link },
            { "cursor-pointer": onClick && !disabled },
            { "icon-medium": medium },
            { "icon-big": big }
          )}
          style={style}
        />
      </a>
    </span>
  );
};

Icon.defaultProps = {
id: undefined,
name:"home",
style: {backgroundColor : "transparent"},
onClick: undefined,
disabled: false,
light: false,
link: true,
medium:false,
big: false
};

export default Icon;
