import React, { useState } from "react";
import "./Home.scss";
import VideoComponent from "../Video/videoComponent.tsx";
import Header from "../Header/Header.tsx";
import TShirtsAndPolos from "../Shop/TShirtsAndPolos.tsx";
import Shoes from "../Shop/Shoes.tsx";
import Accessoires from "../Shop/Accessoires.tsx";
import Sweatshirts from "../Shop/Sweatshirts.tsx";
import Jeans from "../Shop/Jeans.tsx";
import Shorts from "../Shop/Shorts.tsx";
import Joggings from "../Shop/Joggings.tsx";
import Bottes from "../Shop/Bottes.tsx";
interface HomeProps {}

const Home = ({}: HomeProps) => {
  const [selectedChoice, setSelectedChoice] = useState<string | null>(null);

  const handleChoiceSelection = (choice: string) => {
    setSelectedChoice(choice);
    setDisplayHome(false)
  };

  const SelectedComponent = ({ choice }: { choice: string }) => {
    console.log("🚀 ~ SelectedComponent ~ choice:", choice)
    switch (choice) {
      case "T-shirts & Polos":
        return <TShirtsAndPolos />;
      case "Sweats & Hoodies":
        return <Sweatshirts />;
      case "Jeans":
        return <Jeans />;
      case "Shorts":
        return <Shorts />;
      case "Joggings":
        return <Joggings />;
      case "Boots":
        return <Bottes />;
      case "Shoes":
        return <Shoes />;
      case "Accessories":
        return <Accessoires />;
      default:
        return null;
    }
  };

  const [diplayHome, setDisplayHome] = useState(false);

  const goHome = () => {
    setDisplayHome(true);
  };

  return (
    <React.Fragment>
      <Header onSelectChoice={handleChoiceSelection} goHome={goHome} />
      {(!selectedChoice || diplayHome) && <VideoComponent />}
      {(selectedChoice && !diplayHome) && <SelectedComponent choice={selectedChoice} />}
    </React.Fragment>
  );
};

export default Home;
