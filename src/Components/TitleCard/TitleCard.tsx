import React from 'react';
import "./TitleCard.scss";

interface TitleCardProps {
  text : any
  cart? : any
}

const TitleCard = ( {text , cart} : TitleCardProps) => {
  return (
    <div className={cart ? "title-card-container-SC" : "title-card-container"}>
      <p className='title-card-text'>
        {text}
      </p>
    </div>
  );
};

export default TitleCard;