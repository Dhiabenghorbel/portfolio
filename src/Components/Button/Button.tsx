import React, { Children } from "react";
import "./Button.scss";

interface ButtonProps {
  backgroundColor: string;
  borderColor: string;
  border: string;
  radius: boolean;
  text: string;
  textColor: string;
  onClick: any;
}

const Button = ({
  backgroundColor,
  borderColor,
  radius,
  border,
  text,
  textColor,
  onClick,
}: ButtonProps) => {
  const buttonStyle = {
    backgroundColor: backgroundColor ? backgroundColor : "transparent",
    color: textColor ? textColor : "#48C9B0",
    fontWeight: "bold",
    padding: "20px 27px",
    border: border,
    borderRadius: radius ? "35px" : "0px",
    borderColor: borderColor ? borderColor : "#48C9B0",
    cursor: "pointer",
  };

  return (
    <button style={buttonStyle} onClick={onClick}>
      {text}
    </button>
  );
};

export default Button;
