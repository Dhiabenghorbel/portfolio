// Panier.js

import React, { useState, useEffect } from 'react';
import axios from 'axios';
import "./ShoppingCart.scss"
import Modal from '../Modal/Modal.tsx';
import Icon from '../Icon/Icon.tsx';
import TitleCard from '../TitleCard/TitleCard.tsx';

const ShoppingCart = () => {

  const [elements, setElements] = useState([]);
  const [itemDeleted, setItemDeleted] = useState(false);
  const [totalAmount, setTotalAmount] = useState(0);

  const deleteItem = (element: any) => {
    console.log("🚀 ~ deleteItem ~ element:", element)
    axios.delete(`http://localhost:5000/api/paniers/delete/${element._id}`)
      .then(response => {
        console.log("🚀 ~ deleteItem ~ response:", response)

        setItemDeleted(true)
        console.log('success fetching data: ', response);

      })
      .catch(error => {
        console.error('Error fetching data: ', error);
      });
  }
  useEffect(() => {
    axios.get('http://localhost:5000/api/paniers')
      .then(response => {
        setElements(response.data);
        const total = response.data.reduce((acc, cur) => acc + parseInt(cur.price), 0);
        setTotalAmount(total);
      })
      .catch(error => {
        console.error('Error fetching data: ', error);
      });
  }, [itemDeleted , elements]);



  return (
    <Modal>
      <div className="panier-container">
        <TitleCard text="Shopping Cart" cart />
        <div className="element-list" style={{ width: '450px', maxHeight: '400px', overflowY: 'scroll' }}>
          {elements.map(element => (
            <div key={element._id} className="element">
              <img src={element.imageUrl} alt={element.imageUrl} />
              <div className="element-info">
                <p>
                  Brand :
                  <span className='element-value'>
                    {element.brand}
                  </span>
                </p>
                <p>
                  Price :
                  <span className='element-value'>
                    {element.price} $
                  </span>
                </p>
                <div className='delete-item'>
                  <Icon onClick={() => deleteItem(element)} name="fa-solid fa-trash-can" />
                </div>
              </div>
            </div>
          ))}
        </div>
        <div className='total-amount'>
            Total : {totalAmount} $
        </div>
      </div>
    </Modal>
  );
};

export default ShoppingCart;
