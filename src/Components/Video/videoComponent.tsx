import React from 'react';
import "./Video.scss";

const VideoComponent = () => {
  return (
    <div className="video-container">
      <video controls width="1200" autoPlay>
        <source src={process.env.PUBLIC_URL + '/VIDEO.mp4'} type="video/mp4" />
      </video>
    </div>
  );
};

export default VideoComponent;