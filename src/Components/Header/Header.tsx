import React , {useState} from "react";
import "./Header.scss";
// import HeaderBar from "./HeaderBar.tsx";
import HeaderTitles from "./HeaderTitles.tsx";
import Icon from "../Icon/Icon.tsx";
import logo1 from '../../media/logo1.JPG';
// import { Link } from "react-router-dom";
interface HeaderProps {
  onSelectChoice: (choice: string) => void;
  goHome: any;

}

const Header =({ onSelectChoice , goHome }: HeaderProps) => {
  const [isClosed, CloseHeaderBar]: [boolean, Function] = useState(false);

  return (
    <React.Fragment>
      {!isClosed ? (
        <React.Fragment>
          <div className={"header-bar-toolbar"}>
            <div className={"moving-text"}>
              Welcome to CHICLICK! Explore Our World of Fashion ...
            </div>
            <div className={"close-header-bar"}>
              <Icon name="fa-solid fa-xmark" medium onClick={CloseHeaderBar} />
            </div>
          </div>
        </React.Fragment>
      ) : (
        <React.Fragment />
      )}
      <div className={!isClosed ? "header-toolbar" : "header-toolbar2"}>
        <div onClick={() => goHome()}>
        <img  className="logo" src={logo1} alt="logo"/>
        </div>
        <HeaderTitles onSelectChoice={onSelectChoice} />
      </div>
    </React.Fragment>
  );
};

export default Header;
