import React, { useState } from "react";
import "./Header.scss";
import Icon from "../Icon/Icon.tsx";
import ShoppingCart from "../ShoppingCart/ShoppingCart.tsx";

interface HeaderTitlesProps {
  onSelectChoice: (choice: string) => void;
}

const HeaderTitles = ({ onSelectChoice }: HeaderTitlesProps) => {

  const scrollToAboutUs = () => {
    window.scrollTo(0, 1000);
  };
  const [isClicked, setIsClicked] = useState(false);
  const [isOpen, setIsOpen] = useState(false);

  const choices: any = [
    "T-shirts & Polos",
    "Sweats & Hoodies",
    "Jeans",
    "Shorts",
    "Joggings",
    "Boots",
    "Shoes",
    "Accessories",
  ];
  return (
    <React.Fragment>
      <div className={"header-titles"}>
        <div>
          <div
            className="header-titles-text"
            // onMouseEnter={() => setIsHovered(true)}
            onClick={
              isClicked ? () => setIsClicked(false) : () => setIsClicked(true)
            }
          >
            Shop
          </div>
          {isClicked && (
            <div
              style={{
                position: "fixed",
                top: "130px", // Adjust as needed
                left: "0",
                backgroundColor: "white",
                boxShadow: "0 2px 4px rgba(0,0,0,0.2)",
                padding: "10px",
                width: "450px",
                zIndex: "9999",
                marginLeft: "75rem",
              }}
            >
              <div style={{ display: "flex", marginBottom: "10px" }}>
                {choices.slice(0, 3).map((item, index) => (
                  <p
                    className="choices"
                    key={index}
                    style={{ marginRight: "30px" }}
                    onClick={() => {
                      onSelectChoice(item);
                      setIsClicked(false);
                    }}
                  >
                    {item}
                  </p>
                ))}
              </div>
              <div style={{ display: "flex", marginBottom: "10px" }}>
                {choices.slice(3, 6).map((item, index) => (
                  <p
                    className="choices"
                    key={index}
                    style={{ marginRight: "30px" }}
                    onClick={() => {
                      onSelectChoice(item);
                      setIsClicked(false);
                    }}
                  >
                    {item}
                  </p>
                ))}
              </div>
              <div style={{ display: "flex", marginBottom: "10px" }}>
                {choices.slice(6, 9).map((item, index) => (
                  <p
                    className="choices"
                    key={index}
                    style={{ marginRight: "30px" }}
                    onClick={() => {
                      onSelectChoice(item);
                      setIsClicked(false);
                    }}
                  >
                    {item}
                  </p>
                ))}
              </div>
            </div>
          )}
        </div>
        <div className={"header-titles-text"} onClick={scrollToAboutUs}>
          About Us
        </div>
        <div className={"header-titles-text"} onClick={
          isOpen ? () => setIsOpen(false) : () => setIsOpen(true)
        }>
          Shopping Cart
        </div>
      </div>
      {isOpen &&<div
        style={{
          position: "fixed",
          top: "132px",
          backgroundColor: "white",
          boxShadow: "0 2px 4px rgba(0,0,0,0.2)",
          padding: "10px",
          width: "350px",
          zIndex: "999",
          marginLeft: "95rem"
        }}
      >
         <ShoppingCart />
      </div>}
    </React.Fragment>
  );
};

export default HeaderTitles;
