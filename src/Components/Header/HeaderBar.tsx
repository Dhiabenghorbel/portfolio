import React, { useState } from "react";
import "./Header.scss";
import Icon from "../Icon/Icon.tsx";

interface HeaderBarProps {}

const HeaderBar = ({}: HeaderBarProps) => {
  const [isClosed, CloseHeaderBar]: [boolean, Function] = useState(false);
  return (
    <React.Fragment>
      {!isClosed ? (
        <React.Fragment>
          <div className={"header-bar-toolbar"}>
            <div className={"moving-text"}>
              Welcome to my Portfolio ! Explore My World of Creativity ...
            </div>
          </div>
        </React.Fragment>
      ) : (
        <React.Fragment />
      )}
    </React.Fragment>
  );
};

export default HeaderBar;
