import React from 'react';
import './User.scss';

interface UserAvatarProps {
  photo : string;
}

const UserAvatar = ({photo}: UserAvatarProps) => {

    return (
        <div >
            <img className='imgCircle' src={photo} alt="Avatar"/>
        </div>
    );
};

UserAvatar.defaultProps = {
    photo : "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQcbyVSNS3OVI8SnCtueXUaKPS72Ybc7J5K9H2eyf0Rf0qSxtpKaJj_CtKpPSi__pE8FPg&usqp=CAU"
}

export default UserAvatar;
