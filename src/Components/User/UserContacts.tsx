import React from 'react';
import './User.scss';
import Icon from '../Icon/Icon.tsx';

interface UserContactsProps {
  
}

const UserContacts = ({}: UserContactsProps) => {

    return (
    <div className="contact-container">
      <div className="contact" >
         name="fa-solid fa-envelope" big src="" />
      </div>
      <div className="contact">
        <Icon name="fa-brands fa-square-facebook" big src="https://www.facebook.com/dhia.benghorbel.52/"/>
      </div>
      <div className="contact">
        <Icon name="fa-brands fa-instagram" big src="https://www.instagram.com/dhiabenghorbel/"/>
      </div>
      <div className="contact">
        <Icon name="fa-brands fa-square-github" big src="" />
      </div>
      <div className="contact">
        <Icon name="fa-solid fa-download" big src="./Dhia Eddine Ben Ghorbel - Curriculum Vitae.pdf" download  />
      </div>
    </div>
    );
};

UserContacts.defaultProps = {
}

export default UserContacts;
