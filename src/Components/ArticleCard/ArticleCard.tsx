import React, { useState } from 'react';
import './ArticleCard.scss'; // Import your CSS file
import Icon from '../Icon/Icon.tsx';
import axios from 'axios';
import SnackbarToast from '../SnackbarToast/SnackbarToast.tsx';


interface ArticleCardProps {
    data: any
}
const ArticleCard = ({ data }: ArticleCardProps) => {

    const [open, setOpen] = useState(false);

    const closeSnackbarToast = () => {
        setOpen(false);
    }

    const addToShoppingCart = (element: any) => {
        axios.post('http://localhost:5000/api/paniers', element)
            .then(response => {
                setOpen(true)
            })
            .catch(error => {
                console.error('Error fetching data: ', error);
            });
    }

    return (
        <React.Fragment>
            <div className="card-container">
                {
                    data.map((element, index) => (
                        <div className="card" key={index}>
                            <div>
                                <img src={element.imageUrl} alt="Article" />
                                <span className="brand">{element.brand}</span>-
                                <span className="price">${element.price}</span>
                                <div>
                                    <button className="add-to-cart-btn" onClick={() => addToShoppingCart(element)}>
                                        Add to Shopping Cart
                                        <Icon light name="fa-solid fa-cart-shopping" />
                                    </button>
                                </div>
                            </div>
                        </div >
                    ))
                }
            </div>
            <SnackbarToast open={open} message="Item successfully added to your cart !" onClose={closeSnackbarToast} />
        </React.Fragment>
    );
};



export default ArticleCard;