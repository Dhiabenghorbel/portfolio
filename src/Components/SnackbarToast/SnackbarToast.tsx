import React, { Fragment, ReactNode, useEffect } from "react";
import Icon from "../Icon/Icon.tsx";
import "./SnackbarToast.scss";

interface SnackbarToastProps {
  open: boolean;
  onClose: Function;
  message: ReactNode;
}

const SnackbarToast = ({ open, onClose, message }: SnackbarToastProps) => {
  useEffect(() => {
    const timeout = setTimeout(() => onClose(), 10000);
    return () => {
      clearTimeout(timeout);
    };
  }, [open]);

  return (
    open ? <div className={"snackbar-container success"} >
      <p className="snackbar-message">{message}</p>
      <Icon onClick={() => onClose()} name="fa-solid fa-xmark" light />
    </div>
      : <React.Fragment/>
  )
};

SnackbarToast.defaultProps = {
  open: false,
  onClose: () => { },
  message: <Fragment />,

};

export default SnackbarToast;
