import React, { ReactNode } from "react";
import "./Modal.scss";

interface ModalProps {
  children: ReactNode;
}

const Modal = ({ children }: ModalProps) => {

  return (
    <div
      className="modal"
    >
      <div className="full-width">
        {children}
      </div>
    </div>
  );
};

Modal.defaultProps = {
  children: <React.Fragment />,
};

export default Modal;
