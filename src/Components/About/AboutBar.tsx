import React from "react";
import "./About.scss";
import "../Box/Box.scss";
import GoogleMap from "../Maps/GoogleMap.tsx";
import logo1 from '../../media/logo1.JPG';


interface AboutBarProps { }

const AboutBar = ({ }: AboutBarProps) => {
  return (
    <div className="about-bar">
      <p className="about-description" >
        Elevate your style with CHICKCLIK.
        <br/> 
        <br/> 
        Discover the latest trends in fashion and shop our curated collection of clothing and accessories. 
        <br/>
        Stay ahead of the fashion curve with our stylish and affordable range. 
        <br/>
        <br/> 
        Your wardrobe upgrade starts here.
      </p>

      <div>
      <img  className="about-logo" src={logo1} alt="logo"/>
      <p style={{ color: "white"}} >© 2024 CHICLICK with ♥</p>
      </div>
      <div className="about-maps">
        <GoogleMap />
      </div>

    </div>
  );
};

export default AboutBar;
