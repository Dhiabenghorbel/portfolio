import React from "react";
import "./Box.scss";

interface BoxProps {
    children?: any,
}

const Box = ({ children }: BoxProps) => {
  return (
    <React.Fragment>
      <div className={"Box"}>
        {children} 
      </div>
    </React.Fragment>
  );
};

export default Box;
