import React, { useEffect, useState } from "react";
import "../Box/Box.scss";
import ArticleCard from "../ArticleCard/ArticleCard.tsx";
import TitleCard from "../TitleCard/TitleCard.tsx";

interface BoxProps {
}

const Box = ({ }: BoxProps) => {
  const [data, setData] = useState([]);

  useEffect(() => {
    fetch('http://localhost:5000/api/sweatshirts')
      .then(response => {
        console.log("🚀 ~ useEffect ~ response:", response)
        return response.json()
      })
      .then(data => {
        console.log("🚀 ~ useEffect ~ data:", data)
        return setData(data)
      })
      .catch(error => console.error('Error fetching data:', error));

  }, []);

  return (
    <React.Fragment>
      <TitleCard text="- Sweatshirts -" />
      <div className={"Box1"}>
        <ArticleCard data={data} />
      </div>
    </React.Fragment>
  );
};

export default Box;
